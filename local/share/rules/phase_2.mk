### phase_2.mk --- 
## 
## Filename: phase_2.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed May  8 12:38:20 2013 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:


context prj/454_preprocessing
context prj/454_assembly

## all from from *_assembly, MIRA fold
extern ../phase_1/$(PRJ)_assembly/$(PRJ)_d_results/$(PRJ)_out.unpadded.fasta as AS_UPAD_FA

extern ../phase_1/$(PRJ)_assembly/$(PRJ)_d_results/$(PRJ)_out.unpadded.fasta.qual as AS_UPAD_QA

extern ../phase_1/$(PRJ)_assembly/$(PRJ)_d_info/$(PRJ)_info_contigreadlist.txt as AS_CORE_LST

extern ../phase_1/$(PRJ)_assembly/$(PRJ)_d_info/$(PRJ)_info_contigstats.txt as AS_COSTAT_LST


$(PRJ)_as_unpad.fasta: $(AS_UPAD_FA)
	ln -sf $< $@

$(PRJ)_as_unpad.fasta.qual: $(AS_UPAD_QA)
	ln -sf $< $@

$(PRJ)_as_core.lst: $(AS_CORE_LST)
	ln -sf $< $@

$(PRJ)_as_costat.lst: $(AS_COSTAT_LST)
	ln -sf $< $@


# number of reads per contigs from MIRA contigreadlist
# select singletons (only one list)
$(PRJ)_singletons.lst: $(PRJ)_as_core.lst
	bawk -v cov=1 'BEGIN { i=0; j=0;} !/^[$$,\#+]/ \
	{ \
	# check at least 1 reads \
	if ($$2 == "") { exit; }; \
	# initialize with first value \
	if (j == 0 ) { frst == $$1 }; \
	if(frst != $$1) \
	{ \
	# coverage choice \
	if (i == cov) \
	{ \
	print frst, i; \
	} \
	frst=$$1; \
	i=0; \
	} \
	i++; \
	j++; \
	} END { \
	# print last singleton \
	if (i == 1) { print frst, i; } \
	}' $< > $@


# number of reads per contigs from MIRA contigreadlist
# select contigs (at least 2 reads)
$(PRJ)_no_singletons.lst: $(PRJ)_as_core.lst
	bawk -v cov=1 'BEGIN { i=0; j=0;} !/^[$$,\#+]/ \
	{ \
	# check at least 1 reads \
	if ($$2 == "") { exit; }; \
	# initialize with first value \
	if (j == 0 ) { frst == $$1 }; \
	if(frst != $$1) \
	{ \
	# coverage choice DIFFERENCE \
	if (i > cov) \
	{ \
	print frst, i; \
	} \
	frst=$$1; \
	i=0; \
	} \
	i++; \
	j++; \
	} END { \
	# print last contig DIFFERENCE \
	if (i > 1) { print frst, i; } \
	}' $< > $@


$(PRJ)_as_unpad_sing.fasta.gz: $(PRJ)_singletons.lst $(PRJ)_as_unpad.fasta
	get_fasta -f <(cut -f 1 $<) <$^2 | gzip >$@

$(PRJ)_as_unpad_sing.fasta.qual.gz: $(PRJ)_singletons.lst $(PRJ)_as_unpad.fasta.qual
	get_fasta -f <(cut -f 1 $<) <$^2 | gzip >$@


$(PRJ)_as_unpad_nosing.fasta.gz: $(PRJ)_no_singletons.lst $(PRJ)_as_unpad.fasta
	get_fasta -f <(cut -f 1 $<) <$^2 | gzip >$@

$(PRJ)_as_unpad_nosing.fasta.qual.gz: $(PRJ)_no_singletons.lst $(PRJ)_as_unpad.fasta.qual
	get_fasta -f <(cut -f 1 $<) <$^2 | gzip >$@


# get statistic about GC and total length singletons
$(PRJ)_as_unpad_sing.info: $(PRJ)_as_unpad_sing.fasta.gz $(PRJ)_as_unpad_sing.fasta.qual.gz $(PRJ)_singletons.lst
	$(call get_info,$<,$^2,$^3)


# get statistic about GC and total length contigs
$(PRJ)_as_unpad_nosing.info: $(PRJ)_as_unpad_nosing.fasta.gz $(PRJ)_as_unpad_nosing.fasta.qual.gz $(PRJ)_no_singletons.lst
	$(call get_info,$<,$^2,$^3)


$(PRJ)_as_unpad.%.gz: $(PRJ)_as_unpad.%
	gzip <$< >$@

# get statistic about GC and total length all
$(PRJ)_as_unpad.info: $(PRJ)_as_unpad.fasta.gz $(PRJ)_as_unpad.fasta.qual.gz $(PRJ)_no_singletons.lst $(PRJ)_singletons.lst
	cat $^3 $^4 > $(PRJ)_all.lst && \
	$(call get_info,$<,$^2,$(PRJ)_all.lst)



# $(call get_info, fasta, qual, contig reads_num)
define get_info
paste \
<(count_fasta -i 50 <(zcat <$1) ) \
<(zcat <$1 | fastalen | \
stat_base --mean 2 | \
bawk '!/^[$$,\#+]/ { \
{ print "mean_length",$$0; } \
}') \
<(zcat <$1 | fastalen | \
stat_base --stdev 2 | \
bawk '!/^[$$,\#+]/ { \
{ print "standard_deviation_length",$$0; } \
}') \
<(zcat <$1 | fastalen | \
stat_base --median 2 | \
bawk '!/^[$$,\#+]/ { \
{ print "median_length",$$0; } \
}') \
<(zcat <$2 | qual_mean --total | \
bawk '!/^[$$,\#+]/ { \
{ print "mean_quality",$$0; } \
}') \
<(stat_base --mean 2 <$3 | \
bawk '!/^[$$,\#+]/ { \
{ print "mean_reads_cov",$$0; } \
}') > $@
endef



# get contig len>=100 qual>=30
# script help criptic; to be rebuild
$(PRJ)_as_unpad_200.info: $(PRJ)_as_unpad.fasta $(PRJ)_as_unpad.fasta.qual
	filter_by_quality_and_len -q 30 -l 200 -c $< >$@



$(PRJ)_as_unpad_distribplot_len.pdf: $(PRJ)_as_unpad_nosing.fasta.gz $(PRJ)_as_unpad_sing.fasta.gz
	paste <(zcat <$< | fastalen | cut -f2) \
	<(zcat <$^2 | fastalen | cut -f2) \
	| qual_distrib_plot --type histogram --bin-size=150 --remove-na --title="Length distribution of assembled sequences" --xlab="length" --ylab="sequences (#)" --legend-label="contigs","singletons" --color="blue","red" --output $@ 1 2



$(PRJ)_as_unpad_distribplot_qual.pdf: $(PRJ)_as_unpad_nosing.fasta.qual.gz $(PRJ)_as_unpad_sing.fasta.qual.gz
	paste <(zcat $< | qual_mean | cut -f2) \
	<(zcat $^2 | qual_mean | cut -f2) \
	| qual_distrib_plot --type histogram --bin-size=5 --remove-na --title="Distribution of average assembly quality" --xlab="phred quality" --ylab="sequences (#)" --legend-label="contigs","singletons" --color="blue","red" --output $@ 1 2



$(PRJ)_as_unpad_distribplot_cov.pdf: $(PRJ)_no_singletons.lst
	cut -f2 $< \
	| distrib_plot --type histogram --breaks 10 --remove-na --output $@ 1




$(PRJ)_as_unpad_scatterplot_len-qual.pdf: $(PRJ)_as_unpad_nosing.fasta.gz $(PRJ)_as_unpad_nosing.fasta.qual.gz $(PRJ)_as_unpad_sing.fasta.gz $(PRJ)_as_unpad_sing.fasta.qual.gz
	paste <(zcat <$< | fastalen | bsort --key=1,1 | cut -f2) \
	<(zcat <$^2 | qual_mean | bsort --key=1,1 | cut -f2) \
	<(zcat <$^3 | fastalen | bsort --key=1,1 | cut -f2) \
	<(zcat <$^4 | qual_mean | bsort --key=1,1 | cut -f2) | \
	this_scatterplot --no-header --xlab="length (bp)" --ylab="av.qual (phred)" --title="Contigs length versus average quality" --color="red","blue" --pch 1,1 --legend-label="contigs","singletons" --output-file=$@ 1,2 3,4


$(PRJ)_as_unpad_nosing_scatterplot_len-cov.pdf: $(PRJ)_as_unpad_nosing.fasta.gz $(PRJ)_no_singletons.lst
	paste <(zcat $< | fastalen | bsort --key=1,1 | cut -f2) \
	<(bsort --key=1,1 <$^2 | cut -f2) | \
	this_scatterplot --no-header --xlab="length (bp)" --ylab="reads (#)" --title="Contigs length versus number of reads per contig" --color="red" --pch 1 --legend-label="contigs" --output-file=$@ 1,2


$(PRJ)_as_unpad_nosing_scatterplot_cov-qual.pdf: $(PRJ)_as_unpad_nosing.fasta.qual.gz $(PRJ)_no_singletons.lst
	paste <(bsort --key=1,1 <$^2 | cut -f2) \
	<(zcat <$< | qual_mean | bsort --key=1,1 | cut -f2) | \
	this_scatterplot --no-header --ylab="av.qual (phred)" --xlab="reads (#)" --title="Contigs average quality versus number of reads per contig" --color="red" --pch 1 --legend-label="contigs" --output-file=$@ 1,2

###################################

# coverage contigs assembly
$(PRJ)_as_unpad_nosing.cov: $(PRJ)_as_costat.lst
	paste --delimiter "\n" \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$1, $$6; \
	} \
	}' $< | stat_base --mean 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_xbase_cov:",$$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$1, $$6; \
	} \
	}' $< | stat_base --median 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "median_xbase_cov:",$$0; } \
	}') \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) \
	{ \
	print $$1, $$6; \
	} \
	}' $< | stat_base --stdev 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "stdev_xbase_cov:",$$0; } \
	}') > $@

# coverage plot
$(PRJ)_as_unpad_nosing_distribplot_cov.pdf: $(PRJ)_as_costat.lst
	bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) { print $$6; } }' $< \
	| distrib_plot --type histogram --breaks 30 --remove-na --output $@ 1


# prepare file with bins
bins.tab:
	make_bins > $@

# get classification with binner for upload in LibreOffice calc
$(PRJ)_as_unpad_nosing_distribplot_cov.tab: $(PRJ)_as_costat.lst bins.tab
	paste <(bawk '!/^[$$,\#+]/ { \
	if ($$4 > 1) { print $$6; } }' $< \
	| binner --bin-from-file=$^2) > $@





ALL += $(PRJ)_as_unpad.fasta \
       $(PRJ)_as_unpad.fasta.qual \
       $(PRJ)_as_core.lst \
       $(PRJ)_singletons.lst \
       $(PRJ)_no_singletons.lst \
       $(PRJ)_as_unpad_sing.fasta.gz \
       $(PRJ)_as_unpad_sing.fasta.qual.gz \
       $(PRJ)_as_unpad_nosing.fasta.gz \
       $(PRJ)_as_unpad_nosing.fasta.qual.gz \
       $(PRJ)_as_unpad_sing.info \
       $(PRJ)_as_unpad_nosing.info \
       $(PRJ)_as_unpad.info \
       $(PRJ)_as_unpad_distribplot_len.pdf \
       $(PRJ)_as_unpad_distribplot_qual.pdf \
       $(PRJ)_as_unpad_distribplot_cov.pdf \
       $(PRJ)_as_unpad_scatterplot_len-qual.pdf \
       $(PRJ)_as_unpad_nosing_scatterplot_len-cov.pdf \
       $(PRJ)_as_unpad_nosing_scatterplot_cov-qual.pdf \
       $(PRJ)_as_unpad_nosing.cov \
       $(PRJ)_as_unpad_nosing_distribplot_cov.pdf \
       $(PRJ)_as_unpad_nosing_distribplot_cov.tab



INTERMEDIATE += $(PRJ)_as_unpad.fasta.gz \
		$(PRJ)_as_unpad.fasta.qual.gz

CLEAN += $(PRJ)_as_unpad.fasta \
         $(PRJ)_as_unpad.fasta.qual \
         $(PRJ)_as_core.lst \
         $(PRJ)_singletons.lst \
         $(PRJ)_no_singletons.lst \
         $(PRJ)_as_unpad_sing.fasta.gz \
         $(PRJ)_as_unpad_sing.fasta.qual.gz \
         $(PRJ)_as_unpad_nosing.fasta.gz \
         $(PRJ)_as_unpad_nosing.fasta.qual.gz \
         $(PRJ)_as_unpad_sing.info \
         $(PRJ)_as_unpad_nosing.info \
         $(PRJ)_as_unpad.info \
         $(PRJ)_as_unpad_distribplot_len.pdf \
         $(PRJ)_as_unpad_distribplot_qual.pdf \
         $(PRJ)_as_unpad_distribplot_cov.pdf \
         $(PRJ)_as_unpad_scatterplot_len-qual.pdf \
         $(PRJ)_as_unpad_nosing_scatterplot_len-cov.pdf \
         $(PRJ)_as_unpad_nosing_scatterplot_cov-qual.pdf \
	 $(PRJ)_as_unpad_nosing.cov \
	 $(PRJ)_as_unpad_nosing_distribplot_cov.pdf \
	 $(PRJ)_as_unpad_nosing_distribplot_cov.tab



######################################################################
### phase_2.mk ends here
