### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Apr 30 15:51:27 2013 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

context prj/454_preprocessing/

PRJ ?= test

STRAINS ?= ACDNA LCDNA LCDNA_SRA

SEQ ?= ../../../../454_preprocessing_2/dataset/acdna/phase_2/query.deconseq_clean.fq \
       ../../../../454_preprocessing_2/dataset/lcdna/phase_2/query.deconseq_clean.fq \
       ../../../../454_preprocessing_2/dataset/lcdna_sra/phase_2/query.deconseq_clean.fq
QUAL ?=
STRAIN ?= ../../../../454_preprocessing_2/dataset/acdna/phase_2/query.deconseq_clean.strain \
	  ../../../../454_preprocessing_2/dataset/lcdna/phase_2/query.deconseq_clean.strain \
          ../../../../454_preprocessing_2/dataset/lcdna_sra/phase_2/query.deconseq_clean.strain
XML ?=


# associate each name defined in $(STRAINS) with corresponding external link for:
# SEQ QUAL STRAIN XML
# generate a makefile that is subsequently evaluated
links.mk:
	>$@; \
	array1=($(STRAINS)); \
	array2=($(SEQ)); \
	array3=($(QUAL)); \
	array4=($(STRAIN)); \
	array5=($(XML)); \
	for i in `seq 1 $${#array1[@]}`; do \
	if (( $${#array2[@]} > 0 )); then \
	echo -e "extern $${array2[$$i-1]} as $${array1[$$i-1]}_SEQ" >>$@; \
	fi; \
	if (( $${#array3[@]} > 0 )); then \
	echo -e "extern $${array3[$$i-1]} as $${array1[$$i-1]}_QUAL" >>$@; \
	fi; \
	if (( $${#array4[@]} > 0 )); then \
	echo -e "extern $${array4[$$i-1]} as $${array1[$$i-1]}_STRAIN" >>$@; \
	fi; \
	if (( $${#array5[@]} > 0 )); then \
	echo -e "extern $${array5[$$i-1]} as $${array1[$$i-1]}_XML" >>$@; \
	fi; \
	done


include links.mk




# link original data in fasta format
.SECONDEXPANSION: $(addsuffix .fastq, $(STRAINS)) $(addsuffix .strain, $(STRAINS))

%.fastq: $$(%_SEQ)
	ln -sf $< $@

%.qual: $$(%_QUAL)
	ln -sf $< $@


%.strain: $$(%_STRAIN)
	ln -sf $< $@

%.xml: $$(%_XML)
	ln -sf $< $@




# prepare MIRA input files
$(PRJ)_in.454.fastq: $(addsuffix .fastq, $(STRAINS))
	cat $^ > $@


$(PRJ)_straindata_in.txt: $(addsuffix .strain, $(STRAINS))
	cat $^ > $@


$(PRJ)_traceinfo_in.454.xml: $(addsuffix .xml, $(STRAINS))
	merge_trace_xmls $^ -o $@




# join together lengths for every fasta
# be sure that the longest columns is the first (transpose, sort by line length, restranspose)
cleaned_nb_len_distr.pdf: $(addsuffix .fastq, $(STRAINS)) $(PRJ)_in.454.fastq
	paste <(fastq2tab <$< | cut -f 1,2 | bsort --key=1,1 | tab2fasta 2 | fastalen | cut -f2) \
	<(cat $^2 $^3 | fastq2tab | cut -f 1,2 | bsort --key=1,1 | tab2fasta 2 | fastalen | cut -f2) \
	<(fastq2tab <$^4 | cut -f 1,2 | bsort --key=1,1 | tab2fasta 2 | fastalen | cut -f2) | \
	transpose --skip-missing | \
	bsort -V | \
	transpose --skip-missing | \
	this_distrib_plot --type histogram --remove-na --bin-size=25 --title="Length distribution of cleaned reads" --xlab="length" --ylab="reads (#)" --legend-label="TOTAL","LCDNA","ACDNA" --color=black,blue,red --output $@ 2 1 3





# avoid parallel execution targets
.NOTPARALLEL: assembly.log


# assembly
.PRECIOUS: assembly.log
assembly.log: $(PRJ)_in.454.fastq $(PRJ)_straindata_in.txt
	!threads
	$(call assembly_param, $@)




.PHONY: test
test:
	@echo $(SEQ)



ALL += $(PRJ)_in.454.fastq \
       $(PRJ)_straindata_in.txt \
       assembly.log



INTERMEDIATE +=

CLEAN +=



######################################################################
### phase_1.mk ends here
